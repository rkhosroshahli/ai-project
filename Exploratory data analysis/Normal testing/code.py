import matplotlib.pyplot as plt
import pandas as pd
import statsmodels.api as sm

df_titanic = pd.read_csv('titanic.csv')

to_plot = df_titanic['Age'][df_titanic['Age'].notna()].values

plt.figure(figsize=(16, 6))
plt.title('Histogram of Age')

plt.hist(to_plot)

plt.xlabel('Age')
plt.ylabel('Frequency')
plt.show()

plt.figure(figsize=(16, 6))
plt.title('Q-Q Plot for Age vs. Normal Distribution')

sm.qqplot(to_plot, ax=plt.gca(), fit=True)
plt.plot([-3, 3], [-3, 3], 'k--')

plt.show()
