import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math

df = pd.read_csv('titanic.csv')
ages = df.Age[~df.Age.isna()].values
survived = df['Survived'][~df.Age.isna()].values
sex = df['Sex'][~df.Age.isna()].values

df.groupby(['Sex', 'Survived']).agg({'PassengerId': 'count'})

v_min, v_max = min(ages), max(ages)
bins = np.linspace(v_min, v_max, 50)

plt.figure(figsize=(16, 6), dpi=200)

# Survived
indices = survived == 1
plt.hist(ages[indices], bins=bins, alpha=0.4, density=True, color='green', label='survived')
# Died
indices = survived == 0
plt.hist(ages[indices], bins=bins, alpha=0.4, density=True, color='red', label='died')

plt.legend()
plt.show()

v_min, v_max = min(ages), max(ages)
bins = np.array([0, 10, 20, 45, 60, 80])

plt.figure(figsize=(16, 6), dpi=200)
## Survived
indices = survived == 1
plt.hist(ages[indices], bins=bins, alpha=0.4, density=True, color='green', label='survived', ec='black')
## Died
indices = survived == 0
plt.hist(ages[indices], bins=bins, alpha=0.4, density=True, color='red', label='died', ec='black')
#
plt.legend()
plt.show()

v_min, v_max = min(ages), max(ages)
bins = np.array([0, 10, 20, 45, 60, 80])

# Man vs. Woman

############## Man
plt.figure(figsize=(16, 6), dpi=200)
plt.title('Male')
## Survived
indices = (survived == 1) & (sex == 'male')
plt.hist(ages[indices], bins=bins, alpha=0.4, density=True, color='green', label='survived', ec='black')
## Died
indices = (survived == 0) & (sex == 'male')
plt.hist(ages[indices], bins=bins, alpha=0.4, density=True, color='red', label='died', ec='black')
#
plt.legend()
plt.show()

############## Woman
plt.figure(figsize=(16, 6), dpi=200)
plt.title('Female')
## Survived
indices = (survived == 1) & (sex == 'female')
plt.hist(ages[indices], bins=bins, alpha=0.4, density=True, color='green', label='survived', ec='black')
## Died
indices = (survived == 0) & (sex == 'female')
plt.hist(ages[indices], bins=bins, alpha=0.4, density=True, color='red', label='died', ec='black')
#
plt.legend()
plt.show()

v_min, v_max = min(ages), max(ages)
bins = np.array([0, 10, 20, 45, 60, 80])

plt.figure(figsize=(16, 6), dpi=200)
plt.title('Chance of Survival by Age Category')
## Survived
indices = survived == 1
# print(ages)
freqs_survived, _ = np.histogram(ages[indices], bins=bins)
# print(freqs_survived)
## Died
indices = survived == 0
freqs_died, _ = np.histogram(ages[indices], bins=bins)
# print(freqs_died)
# To Plot
xs = ['{}-{}'.format(bins[i], bins[i + 1]) for i in range(len(bins) - 1)]
to_plot = 100 * freqs_survived / (freqs_survived + freqs_died)
plt.bar(xs, to_plot)
#
plt.xlabel('Age Category')
plt.ylabel('Chance of Survival')
plt.show()


def round_up(n, decimals=0):
    multiplier = 10 ** decimals
    return math.ceil(n * multiplier) / multiplier


def survival_chance(titanic_df, start_age, end_age):
    # male
    indices = (survived == 1) & (sex == 'male')
    bins = np.array([start_age, end_age])
    freqs_survived_m, _ = np.histogram(ages[indices], bins=bins)
    indices = (survived == 0) & (sex == 'male')
    freqs_died_m, _ = np.histogram(ages[indices], bins=bins)
    if (freqs_survived_m[0] == 0 & freqs_died_m[0] == 0):
        male_chance = -1
    else:
        male_chance = round_up(freqs_survived_m / (freqs_survived_m + freqs_died_m), 3)
    # female
    indices = (survived == 1) & (sex == 'female')
    bins = np.array([start_age, end_age])
    freqs_survived_f, _ = np.histogram(ages[indices], bins=bins)
    indices = (survived == 0) & (sex == 'female')
    freqs_died_f, _ = np.histogram(ages[indices], bins=bins)
    if (freqs_survived_f[0] == 0 & freqs_died_f[0] == 0):
        female_chance = -1
    else:
        female_chance = round_up(freqs_survived_f / (freqs_survived_f + freqs_died_f), 3)

    return {'female': female_chance, 'male': male_chance}


assert survival_chance(df, 70, 80) == {'female': -1, 'male': 0.143}
