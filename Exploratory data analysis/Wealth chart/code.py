import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math

wealth_df = pd.read_csv('WealthDistribution.csv', index_col='year').T

plt.figure(dpi=100, figsize=(10, 10))
for index, row in wealth_df.iterrows():
    plt.plot(row[:-4].index, row[:-4].values, label=index)
plt.legend()
plt.title('Distribution of American Family Wealth By Year')
plt.xlabel('Percentile')
plt.ylabel('Wealth dollars')


def calculate_mean(row):
    return np.mean(row)


wealth_df["mean"] = wealth_df.apply(calculate_mean, axis=1)


def calculate_sigma(row):
    diff = 0
    summ = 0
    count = 0
    for i in range(0, 100):
        diff = row.values[i] - row.values[100]
        summ += (diff ** 2)
    divs = summ / 99
    # std = np.std(row[:100])
    std2 = math.sqrt(divs)
    return std2


w_sigma = wealth_df.apply(calculate_sigma, axis=1)
wealth_df['sigma'] = wealth_df.apply(calculate_sigma, axis=1)


def calculate_mu3(row):
    diff = 0
    summ = 0
    for i in range(0, 100):
        diff = row.values[i] - row.values[100]
        summ += diff ** 3
    divs = summ / 100
    return divs


wealth_df['mu3'] = wealth_df.apply(calculate_mu3, axis=1)

wealth_df['skewness'] = wealth_df['mu3'] / (wealth_df['sigma'] ** 3)

plt.plot(wealth_df['skewness'])
plt.title("Pearson's moment coefficient of skewness")

plt.plot((wealth_df['mean'] - wealth_df[50]))
plt.title("mean - median")

wealth_df[['mean', 'sigma', 'mu3', 'skewness']].to_csv('result.csv', index_label='year')
