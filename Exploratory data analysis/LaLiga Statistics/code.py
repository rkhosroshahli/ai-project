import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

df_laliga = pd.read_csv('LaLiga_dataset.csv')

seasons = sorted(df_laliga['season'].unique())[-17::16]
points = np.zeros((len(seasons), 20))
for i, season in enumerate(seasons):
    points[i] = df_laliga[df_laliga['season'] == season]['points'].values

points_min = np.min(points)
points_max = np.max(points)
bins = np.linspace(points_min, points_max, 10)

plt.figure(figsize=(16, 6), dpi=300)
plt.title('Histogram of Points: 2000 vs 2016')

for i, season in enumerate(seasons):
    plt.hist(points[i], bins=bins, alpha=0.4, ec='black', label=season)
#
plt.ylabel('Number of Teams')
plt.xlabel('Points')
plt.legend()
plt.show()

print(df_laliga[df_laliga['season'] == '1970-71'].sort_values("points", ascending=False).head(5))


def min_points(df_laliga, season):
    df_seas = df_laliga[df_laliga['season'] == season]
    return (df_seas[['club', 'points']].head(1).to_dict('records')[0])


assert min_points(df_laliga, '1997-98') == {'club': 'Sporting de Gijon', 'points': 13}


def max_points(df_laliga, season):
    return df_laliga[df_laliga['season'] == season][['club', 'points']].tail(1).to_dict('records')[0]


assert max_points(df_laliga, '1997-98') == {'club': 'Barcelona', 'points': 74}
