import matplotlib.pyplot as plt
import pandas as pd

df_laliga = pd.read_csv('LaLiga_dataset.csv')


def get_season_table(df_laliga, season_name):
    return pd.DataFrame(
        df_laliga[df_laliga['season'] == season_name].sort_values(['points', 'goal_difference'], ascending=False))


get_season_table(df_laliga, '2016-17')

pd.DataFrame.from_dict({'club': {'1970-71': 16,
                                 '1971-72': 18,
                                 '1972-73': 18,
                                 '1973-74': 18,
                                 '1974-75': 18}})

date_unique = pd.unique(df_laliga['season'])
num_teams = []
for date in date_unique:
    num_teams.append(get_season_table(df_laliga, date).shape[0])
club_count = {'': date_unique, 'club': num_teams}
pd.DataFrame(club_count).to_csv('club_count.csv', index=False)

pd.DataFrame.from_dict({'club': {'2012-13': 'Barcelona',
                                 '2013-14': 'Atletico de Madrid',
                                 '2014-15': 'Barcelona',
                                 '2015-16': 'Barcelona',
                                 '2016-17': 'Real Madrid'}})

champ_teams = []
for date in date_unique:
    champ_teams.append(get_season_table(df_laliga, date)['club'].head(1).to_string(index=False).strip())
champions = {'': date_unique, 'club': champ_teams}
pd.DataFrame(champions).to_csv('champions.csv', index=False)

count_champs = {}
champ_teams.sort()
this_team = None
count = 1
for i in range(len(champ_teams)):
    if ((this_team == None) or (this_team != champ_teams[i])):
        count = 1
        this_team = champ_teams[i]
        count_champs.update({str(this_team): 1})
    elif (this_team == champ_teams[i]):
        count += 1
        count_champs.update({str(this_team): count})

championship_count = pd.DataFrame.from_dict({'club': count_champs}).sort_values('club', ascending=False)

pd.DataFrame(championship_count).rename(columns={'club': 'count'}).to_csv('championship_count.csv')
plt.figure()
plt.title('Number of Championships Won for each Team')

plt.bar(championship_count.index.to_list(), championship_count.values[:, 0])

plt.ylabel('Number of Championships Won')
plt.xticks(rotation=75)
plt.show()
