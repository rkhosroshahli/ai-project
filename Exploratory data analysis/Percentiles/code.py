import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

boys = pd.read_csv('boys.csv', sep='\t')
girls = pd.read_csv('girls.csv', sep='\t')

boys_r = boys.rename(columns={'P01': 'P0.1', 'P999': 'P99.9'})
girls_r = girls.rename(columns={'P01': 'P0.1', 'P999': 'P99.9'})

boys_cutted = boys_r.head(10)
xs = boys_cutted['Day']
print(xs)

xs = boys_r['Day']
print(xs)
plt.figure()
plt.plot(xs, ys_linear, 'b.-')

plt.xticks([])
plt.yticks([])
plt.show()


def is_normal(boys_df, girls_df, age_in_days, height_in_cm, gender):
    if (gender == 'male'):
        lowest = boys_df.iloc[age_in_days, 9]
        highest = boys_df.iloc[age_in_days, 13]
    else:
        lowest = girls_df.iloc[age_in_days, 9]
        highest = girls_df.iloc[age_in_days, 13]
    if (height_in_cm < lowest or height_in_cm >= highest):
        return False
    else:
        return True


assert is_normal(boys, girls, 1500, 108.442, 'male')

assert is_normal(boys, girls, 1500, 108.443, 'male') == False
