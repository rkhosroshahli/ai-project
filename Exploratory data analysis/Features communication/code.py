import pandas as pd
import numpy as np
from numpy import unravel_index
import matplotlib.pyplot as plt

df_titanic = pd.read_csv('titanic.csv')

print(df_titanic.corr())

print(df_titanic)

to_plot = df_titanic.corr()
n_columns = len(to_plot)

# Create Figure
plt.figure( figsize=(16,16))

# Add Heatmap
plt.imshow( to_plot.values, cmap = 'RdBu' )

# Set Aesthetics
plt.xticks( list(range(n_columns)), to_plot.columns, rotation = 45 )
plt.yticks( list(range(n_columns)), to_plot.columns, rotation = 45 )
plt.xlim(-0.6, n_columns-0.4 )
plt.ylim(-0.6, n_columns-0.4)
plt.clim(-1,1)
plt.colorbar()
plt.show()


def max_corr(dataframe):
    df_corr = dataframe.corr()
    max_c = i_i = j_j = 0
    for i in range(df_corr.shape[0]):
        for j in range(df_corr.shape[1]):
            if (df_corr.iloc[i, j] < max_c):
                max_c = df_corr.iloc[i, j]
                i_i = i
                j_j = j
    return ({df_corr.columns.values[i_i], df_corr.columns.values[j_j]})

print(max_corr(df_titanic))