import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime
from tqdm import tqdm
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

interactions = pd.read_csv('notifications.csv')

print(interactions)

stime = "01/06/2019"
time_origin = datetime.strptime(stime, "%d/%m/%Y").timestamp()

interactions['interaction_time_absolute'] = interactions['interaction_time'].map(
    lambda x: datetime.fromtimestamp(time_origin + int(x)))

interactions['interaction_hour'] = interactions['interaction_time_absolute'].map(lambda x: x.hour)

interactions['interaction_dow'] = interactions['interaction_time_absolute'].map(lambda x: (datetime.weekday(x) + 2) % 7)

actions = interactions.groupby(['action', 'interaction_hour']).agg({'action': 'count'})
print(actions)

notifs_clicked = np.array(actions.loc['C']['action'].values)
notifs_failed = np.array(actions.loc['F']['action'].values)
notifs_count = notifs_clicked + notifs_failed
plt.scatter(range(24), notifs_clicked, c='g')
plt.scatter(range(24), notifs_failed, c='r')

plt.plot(notifs_count, c='b')
plt.plot(notifs_clicked, c='g')
plt.plot(notifs_failed, c='r')
plt.show()

hit_rate = notifs_clicked / (notifs_clicked + notifs_failed)
plt.plot(hit_rate)
plt.show()

interactions['hour_hit_rate'] = interactions['interaction_hour'].map(lambda x: hit_rate[x])

actions = interactions.groupby(['action', 'interaction_dow']).agg({'action': 'count'})

plt.plot(notifs_count, 'b')
plt.plot(notifs_clicked, c='g')
plt.plot(notifs_failed, c='r')
plt.show()

hit_rate = notifs_clicked / (notifs_clicked + notifs_failed)
plt.plot(hit_rate)
plt.show()

interactions['dow_hit_rate'] = interactions['interaction_dow'].map(lambda x: hit_rate[x])

actions = interactions.groupby(['action', 'application_id']).agg({'action': 'count'})

notifs_clicked = np.array(actions.loc['C']['action'].values)
notifs_failed = np.array(actions.loc['F']['action'].values)
notifs_count = notifs_clicked + notifs_failed

hit_rate = notifs_clicked / (notifs_clicked + notifs_failed)

interactions['app_id_hit_rate'] = interactions['application_id'].map(lambda x: hit_rate[x])

user_ids = np.unique(interactions['user_id'])
print(interactions.head())


def get_user_id_hit_rate(user_id):
    actions = interactions[interactions['user_id'] == user_id]
    f = actions[actions['action'] == 'F'].count().action
    c = actions[actions['action'] == 'C'].count().action
    return c / (f + c)


user_id_hit_rate = {}
for user_id in tqdm(user_ids):
    user_id_hit_rate[user_id] = get_user_id_hit_rate(user_id)

interactions['user_id_hit_rate'] = interactions['user_id'].map(lambda user_id: user_id_hit_rate[user_id])

x = interactions[['hour_hit_rate', 'dow_hit_rate', 'app_id_hit_rate', 'user_id_hit_rate']]
y = interactions['action'].map(lambda x: 1 if x == "C" else 0)

x_std = StandardScaler().fit_transform(x)
pca = PCA(n_components=2, svd_solver='full', random_state=0)
x_pca = pca.fit_transform(x_std)

plt.scatter(x_pca[:, 0], x_pca[:, 1], c=y)

np.save('x_pca.npy', x_pca)
