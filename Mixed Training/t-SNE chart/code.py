from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors
from sklearn import datasets

import mpld3
from mpld3 import plugins

digits = datasets.load_digits()

x = digits.data
y = digits.target


def tsne(x):
    x_tsne = TSNE(n_components=2, init='pca', random_state=0).fit_transform(x)
    return x_tsne


x_tsne = tsne(x)

colors = list(matplotlib.colors.TABLEAU_COLORS)

label_colors = []
for idx in y:
    label_colors.append(colors[idx])

plt.scatter(x_tsne[:, 0], x_tsne[:, 1], c=label_colors)

x_for_plot = []
for i in range(10):
    x_for_plot.append(x_tsne[y == i])

fig, ax = plt.subplots()
ax.grid(True, alpha=0.3)

for i in range(10):
    ax.scatter(x_for_plot[i][:, 0], x_for_plot[i][:, 1], c=label_colors[i], label=i)

# define interactive legend

handles, labels = ax.get_legend_handles_labels()  # return lines and labels
interactive_legend = plugins.InteractiveLegendPlugin(zip(handles,
                                                         ax.collections),
                                                     labels,
                                                     alpha_unsel=0.5,
                                                     alpha_over=1.5,
                                                     start_visible=True)

plugins.connect(fig, interactive_legend)

mpld3.enable_notebook()
mpld3.display()
