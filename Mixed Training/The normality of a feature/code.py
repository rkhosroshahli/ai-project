import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

matplotlib.use('Agg')


def show_plot(column, path=None):
    df_train = pd.read_csv('data/train.csv')

    dft = df_train[column]
    n_dft = (dft - dft.mean()) / dft.std()

    plt.clf()

    normal = np.random.normal(0, 1, 20000)

    percs = np.linspace(0, 100, 70)

    qn_normal = np.percentile(normal, percs)

    # print(np.percentile(dft, percs).shape)

    data = np.array(n_dft)
    qn_data = np.percentile(data, percs)

    x = np.linspace(np.min((qn_normal.min(), qn_data.min())), np.max((qn_normal.max(), qn_data.max())))
    plt.plot(x, x, color="k", ls="--")

    if path:
        plt.savefig(path)
