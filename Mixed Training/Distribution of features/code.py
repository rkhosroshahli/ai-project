import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.stats import iqr

matplotlib.use('Agg')


def show_hist(column, path=None):
    df_train = pd.read_csv('train.csv')

    data = df_train[column]
    # bin_count = int(((iqr(df_train[column], rng=(25, 75), scale="raw", nan_policy="omit") * 2) /np.power(n, 1/3)).round())
    bw = (iqr(df_train[column], rng=(25, 75), scale="raw", nan_policy="omit") * 2) / np.power(df_train.shape[0], 1 / 3)
    datmin, datmax = data.min(), data.max()
    datrng = datmax - datmin
    result = int((datrng / bw) + 1)
    hist = plt.hist(df_train[column], bins=result, color="blue")

    if path:
        plt.savefig(path)

    return hist[0]
