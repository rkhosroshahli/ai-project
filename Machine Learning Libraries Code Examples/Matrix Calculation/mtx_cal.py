import numpy as np

A = np.array([[1, 2], [5, -2]])
B = np.array([[-3, 0], [4, 5]])
C = np.array([[4, 25], [-0.5, -3]])
I = np.identity(2)

C_inv = np.linalg.inv(C)
D = np.dot(A, C_inv)
B_T = B.T

M = D + B_T + (3 * I)
print(M)
