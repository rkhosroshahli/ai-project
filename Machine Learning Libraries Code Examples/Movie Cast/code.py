import pandas as pd

dates = pd.read_csv('dates.csv')

print(dates.describe())

print(dates.head(10))

dates.drop('Unnamed: 0', axis=1)

sample = dates.head(500)

sample.to_csv(path_or_buf='sample.csv')
