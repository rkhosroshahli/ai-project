import pandas as pd

data = pd.read_csv('AQI2018.csv')
aqi_data = pd.DataFrame(data)

most_pol = aqi_data[['Date', 'AQI']]
most_pol = pd.DataFrame(most_pol.groupby('Date', group_keys=True, as_index=False).AQI.mean())

sorted_pol = most_pol.sort_values('AQI', ascending=False).head(10)
sorted_pol.to_csv('dates.csv', header=False, index=False)

states_med = pd.DataFrame(aqi_data.groupby('State Name', as_index=False).AQI.median()).sort_values('AQI',
                                                                                                   ascending=False).head(
    10)
states_med.to_csv('states.csv', index=False, header=False)

sandiego = aqi_data[aqi_data['county Name'] == 'San Diego'].groupby('Defining Parameter',
                                                                    as_index=False).AQI.max().sort_values('AQI',
                                                                                                          ascending=False)
num_PM = aqi_data[(aqi_data['county Name'] == 'San Diego') & (aqi_data['Defining Parameter'] == 'PM2.5')].shape[0]
num_OZ = aqi_data[(aqi_data['county Name'] == 'San Diego') & (aqi_data['Defining Parameter'] == 'Ozone')].shape[0]
num_NO = aqi_data[(aqi_data['county Name'] == 'San Diego') & (aqi_data['Defining Parameter'] == 'NO2')].shape[0]
sandiego['AQI'] = [num_PM, num_OZ, num_NO]
sandiego = sandiego.sort_values('AQI', ascending=False)
sandiego.to_csv('parameters.csv', header=False, index=False)
