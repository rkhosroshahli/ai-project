import numpy as np


def cdf(sample):
    x = np.sort(sample)
    y = 1. * np.arange(len(x) + 1) / len(x)
    return x, y
