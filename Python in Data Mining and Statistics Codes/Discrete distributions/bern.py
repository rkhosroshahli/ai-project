import random


def bern_sim(n, p):
    if p > 1 or p < 0 or n < 1 or (type(n) != int):
        return - 1
    ones = 0
    zeros = 0
    for i in range(n):
        rand = random.random()
        if rand < p:
            ones += 1
        else:
            zeros += 1

    return ones / n
