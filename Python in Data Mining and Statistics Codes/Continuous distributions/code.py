import numpy as np
import matplotlib.pyplot as plt

population = np.load('population.npy')
# hist
plt.figure(figsize=(16, 6), dpi=200)
plt.hist(population, bins=100)
plt.title("Weight Distribution of Sample")
plt.show()

population_sorted = np.sort(population)
y = np.arange(1, len(population) + 1) / len(population)

# cdf
plt.figure(figsize=(16, 6), dpi=200)
plt.plot(population_sorted, y, '.', linestyle='none')
plt.title("Cumulative Distribution Function")
plt.show()

mean = population.sum() / population.size
var = np.var(population)
normal_dist = np.random.normal(mean, var, population.size)
normal_dist_sorted = np.sort(normal_dist)
y_normal_dist = np.arange(1, len(normal_dist) + 1) / len(normal_dist)

plt.figure(figsize=(16, 6))
plt.plot(population_sorted, y, '.', c='blue', linestyle='none', label='Population')
plt.plot(normal_dist_sorted, y_normal_dist, '.', c='red', linestyle='none', label='Normal Distribution')
plt.legend()
plt.show()
