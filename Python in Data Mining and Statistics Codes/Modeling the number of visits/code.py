import numpy as np
import math as m


def calc(last_days, k):
    sumo = last_days.sum()
    land = sumo / last_days.size
    exp = np.exp(-land)
    power = land ** k
    multi = power * exp
    fct = m.factorial(int(k))
    div = multi / fct
    return div


arr = np.array([1, 2, 3])

print(calc(arr, 3))
