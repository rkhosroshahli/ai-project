import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import joblib

df = pd.read_csv('titanic.csv').drop(columns=['PassengerId'])
y = df['Survived']
X = df.drop(columns='Survived')

X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state=42, test_size=0.33)

random_forest = RandomForestClassifier(n_estimators=15)

random_forest.fit(X_train, y_train)

y_pred_random_forest = random_forest.predict(X_test)

acc_random_forest = accuracy_score(y_test, y_pred_random_forest)

print(acc_random_forest)

joblib.dump(random_forest, 'model.pkl')

print(random_forest)
