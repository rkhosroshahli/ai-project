from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.externals.six import StringIO
from IPython.display import Image
from sklearn.tree import export_graphviz
import pydotplus
import pandas as pd

data = pd.read_csv('wbc.csv', index_col='id')
X = data.drop(columns='diagnosis')
y = data['diagnosis'].map({'B': 0, 'M': 1})
X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    test_size=0.2,
                                                    stratify=y,
                                                    random_state=1)
# calling the model
tree_model = DecisionTreeClassifier()
# trainig the data
tree_model.fit(X_train, y_train)

y_predicted = tree_model.predict(X_test)
acc = accuracy_score(y_test, y_predicted)
print(acc)

dot_data = StringIO()
export_graphviz(tree_model, out_file=dot_data,
                filled=True, rounded=True,
                special_characters=True)

graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
Image(graph[0].create_png())
