import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
import joblib
import numpy as np

df = pd.read_csv('magazine.csv', index_col='id')

y = df['Buy']
X = df.drop(columns='Buy')

X_train , X_test, y_train,y_test = train_test_split(X,y,stratify = y ,random_state = 40, test_size=0.2)

logistic = LogisticRegression(random_state = 45)
clf_logistic = logistic.fit(X_train, y_train)
y_pred_logistic = logistic.predict(X_test)
acc_logistic = accuracy_score(y_test,y_pred_logistic)

print(acc_logistic)

print(logistic)

joblib.dump(logistic,'model.pkl')