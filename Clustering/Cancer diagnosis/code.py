import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
import numpy as np
import joblib
import matplotlib.pyplot as plt

df = pd.read_csv('data.csv')

X = df[['compactness_mean', 'radius_mean']].values
Y = df['diagnosis'].map({'B': 0, 'M': 1})

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, stratify=Y, random_state=45)

K = [2, 3, 4, 5, 6, 7, 8, 9, 10]
acc_max = 0
acc_list = []
knn_max = None
for k in K:
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(X_train, Y_train)
    y_pred_knn = knn.predict(X_test)
    acc = accuracy_score(Y_test, y_pred_knn)
    acc_list.append(acc)
    if acc_max < acc:
        knn_max = knn
        acc_max = acc

final_model = knn_max
print(acc_max)
print(final_model)

plt.figure(figsize=(16, 6))
plt.plot(K, acc_list, '-o')
plt.xlabel('number of neighbors, k')
plt.ylabel('accuracy')
plt.xticks(K)
plt.show()

joblib.dump(final_model, 'model.pkl')
