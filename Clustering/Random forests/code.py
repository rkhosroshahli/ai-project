from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('wbc.csv', index_col='id')

y = data['diagnosis'].map({'B': 0, 'M': 1})
X = data.drop(columns='diagnosis')

X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    test_size=0.2,
                                                    stratify=y,
                                                    random_state=1)
rf = RandomForestClassifier(n_estimators=500,
                            min_samples_leaf=0.13,
                            random_state=1)
rf.fit(X_train, y_train)
y_pred_rf = rf.predict(X_test)
acc_rf = accuracy_score(y_test, y_pred_rf)
print(acc_rf)

importances_rf = pd.Series(rf.feature_importances_,
                           index=X.columns)
sorted_importances_rf = importances_rf.sort_values()

plt.figure(figsize=(16, 6), dpi=100)
sorted_importances_rf.plot(kind='barh',
                           color='red', alpha=0.5)
plt.title("Feature Importance")
