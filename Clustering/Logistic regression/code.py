from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

df = pd.read_csv('wbc.csv', index_col='id')
print(df)

y = df['diagnosis'].map({'B': 0, 'M': 1})
X = df.drop(columns='diagnosis')
X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, test_size=0.2)

logistic = LogisticRegression()
clf_logistic = logistic.fit(X_train, y_train)
y_pred_logistic = logistic.predict(X_test)
acc_logistic = accuracy_score(y_test, y_pred_logistic)
print(acc_logistic)
