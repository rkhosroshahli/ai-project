import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.linear_model import LinearRegression


class Linear_Regression:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.alpha0 = 0
        self.alpha1 = 0
        self.coefficients = [self.alpha0, self.alpha1]

    def fit(self, x, y):
        print(type(x))
        self.x = np.array(x)
        self.y = np.array(y)

        self.alpha1 = np.sum((self.x - self.x.mean()) * (self.y - self.y.mean())) / np.sum(
            (self.x - self.x.mean()) ** 2)
        self.alpha0 = (self.y.mean() - (self.alpha1 * self.x.mean()))
        # print(self.alpha0, self.alpha1)

        self.coefficients = [self.alpha0, self.alpha1]
        return self

    def predict(self, x):
        # print(self.alpha0.shape, self.alpha1.shape , x.shape)
        return self.alpha0 + (self.alpha1 * x)

    def mean_squared_error(self, y_test, y_pred):
        # print(((y_test-y_pred)**2).shape)
        return np.sum(((y_test - y_pred) ** 2) / 100)


# Load the diabetes dataset
diabetes = datasets.load_diabetes()

# Use only one feature(BMI)
diabetes_X = diabetes.data[:, np.newaxis, 2]

# Split the data into training/testing sets
n = 100
diabetes_X_train = diabetes_X[:-n].reshape(-1)
# print(diabetes_X_train[341])
diabetes_X_test = diabetes_X[-n:].reshape(-1)

# Split the targets into training/testing sets
diabetes_Y_train = diabetes.target[:-n]
diabetes_Y_test = diabetes.target[-n:]
# print(diabetes_X_test.shape, diabetes_Y_test.shape)
# print(diabetes_X_train.shape, diabetes_Y_train.shape)

# Create linear regression object
regr = Linear_Regression()

# Train the model using the training sets
regr.fit(diabetes_X_train, diabetes_Y_train)

# Make predictions using the testing set
diabetes_Y_pred = regr.predict(diabetes_X_test)

# The coefficients
print('Coefficients: \n', regr.coefficients)

print("Mean squared error: {}".format(regr.mean_squared_error(diabetes_Y_test, diabetes_Y_pred)))

plt.scatter(diabetes_X_test, diabetes_Y_test)
plt.plot(diabetes_X_test, diabetes_Y_pred, c='r', linewidth=2)

np.reshape(diabetes_X_train, (-1, 1))

reg = LinearRegression().fit(np.reshape(diabetes_X_train, (-1, 1)), np.array(diabetes_Y_train))

print(reg.score(np.reshape(diabetes_X_train, (-1, 1)), np.array(diabetes_Y_train)))
print(reg.coef_)
print(reg.intercept_)
print(reg.predict(np.reshape(diabetes_X_test, (-1, 1))))
